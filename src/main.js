import {
    createApp
} from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle.js'
import axios from 'axios'

// axios.defaults.baseURL = 'http://gan-apidev.merahputihacademy.com/api/v1'
axios.defaults.baseURL = 'http://auth.merahputihacademy.com/api/v1'
// const linkAuth = 'http://auth.merahputihacademy.com/api/v1';
// const linkPsikotest = 'http://gan-apidev.merahputihacademy.com/api/v1';
createApp(App).use(store).use(router).mount('#app')