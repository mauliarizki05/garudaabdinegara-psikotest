import {
    createRouter,
    createWebHistory
} from 'vue-router'
import LoginPage from '../views/Login.vue'
import RegisterPage from '../views/Register.vue'
import ChooseProgramPage from '../views/ChooseProgram.vue'
import SuccessRegisterPage from '../views/SuccessRegisterPage.vue'
import ResetPassword from '../views/ResetPassword.vue'
import ChooseSubProgramPage from '../views/ChooseSubProgram.vue'
import StartKecermatan from '../views/Kecermatan/StartTest.vue'
import TestKecermatan from '../views/Kecermatan/TestKecermatan.vue'
import FinishKecermatan from '../views/Kecermatan/Finish.vue'


const routes = [{
        path: '/',
        name: 'login',
        component: LoginPage
    },
    {
        path: '/register',
        name: 'register',
        component: RegisterPage
    },
    {
        path: '/success-register',
        name: 'SuccessRegister',
        component: SuccessRegisterPage
    },
    {
        path: '/choose-program',
        name: 'ChooseProgram',
        component: ChooseProgramPage
    },
 
    {
        path: '/choose-subprogram',
        name: 'ChooseSubProgram',
        component: ChooseSubProgramPage
    },
  
    {
        path: '/kecermatan',
        name: 'StartKecermatan',
        component: StartKecermatan
    },
    {
        path: '/test',
        name: 'TestKecermatan',
        component: TestKecermatan
    },
    {
        path: '/reset',
        name: 'ResetPassword',
        component: ResetPassword
    },
    {
        path: '/selesai',
        name: 'SelesaiTest',
        component: FinishKecermatan
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router